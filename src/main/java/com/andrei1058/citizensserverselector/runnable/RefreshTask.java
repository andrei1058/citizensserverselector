package com.andrei1058.citizensserverselector.runnable;

import com.andrei1058.citizensserverselector.Main;
import org.bukkit.scheduler.BukkitRunnable;

public class RefreshTask extends BukkitRunnable {

    @Override
    public void run() {
        Main.getServersUpdater().updateHolograms();
        Main.getWorldsUpdater().updateHolograms();
    }
}
