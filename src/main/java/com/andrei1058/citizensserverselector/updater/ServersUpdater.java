package com.andrei1058.citizensserverselector.updater;

import com.andrei1058.citizensserverselector.Main;
import com.google.common.collect.Iterables;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ServersUpdater extends PlayerCounter {

    private boolean initialized = false;
    private List<String> availableServers = new ArrayList<>();
    private long lastUpdate = 0L;

    public void init(Player player) {
        if (initialized) return;
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("GetServers");
        } catch (IOException e) {
            e.printStackTrace();
        }

        player.sendPluginMessage(Main.getInstance(), "BungeeCord", b.toByteArray());
    }

    /**
     * Add a new server to be in query
     */
    public void addServer(String name) {
        availableServers.add(name);
    }

    /**
     * Set initialized
     */
    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

    /**
     * Get last update
     */
    public long getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public void updateHolograms() {
        if (Bukkit.getOnlinePlayers().size() == 0) return;
        lastUpdate = System.currentTimeMillis();
        for (String server : availableServers) {
            Player p = Iterables.getFirst(Bukkit.getOnlinePlayers(), null);
            if (p == null) return;
            //noinspection UnstableApiUsage
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("PlayerCount");
            out.writeUTF(server);
            p.sendPluginMessage(Main.getInstance(), "BungeeCord", out.toByteArray());
        }
    }
}
