package com.andrei1058.citizensserverselector.updater;

import java.util.HashMap;

public abstract class PlayerCounter {

    private HashMap<String, Integer> counter = new HashMap<>();

    /**
     * Add a new World/ Server
     */
    public void addCounterKey(String newKey) {
        if (!counter.containsKey(newKey)) {
            counter.put(newKey, 0);
        }
    }

    /**
     * Get integer counter by target key
     */
    @SuppressWarnings("WeakerAccess")
    public HashMap<String, Integer> getCounter() {
        return new HashMap<>(counter);
    }

    /**
     * Update counters
     */
    public void update(String key, Integer value) {
        if (!counter.containsKey(key)) return;
        counter.replace(key, value);
    }

    /** Get a counter value */
    public int getCounter(String key){
        if (!getCounter().containsKey(key)) return 0;
        return getCounter().get(key);
    }

    /** Update NPC holograms*/
    public abstract void updateHolograms();
}
