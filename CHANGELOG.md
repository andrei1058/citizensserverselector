#### 0.3.2
- added new updater
- removed version support interface, using reflection

#### 0.3.1
- compiled with java 8
#### 0.3
- added bStats metrics
- added 1.14 support
- added PlaceholderAPI support
- fixed issue setCounter server1 + server2 was always showing 0

#### 0.2.4

* fixed issue where `setType` was saying "cmd not found" after working successfully.
* fixed issue where you couldn't use plus symbol at `setCounter` command.